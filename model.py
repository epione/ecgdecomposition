import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
from torch.autograd import Variable

class Chomp1d(nn.Module):
    def __init__(self, chomp_size):
        super(Chomp1d, self).__init__()
        self.chomp_size = chomp_size

    def forward(self, x):
        return x[:, :, :-self.chomp_size].contiguous()


class encoder_block(nn.Module):
    def __init__(self, n_inputs, n_outputs, kernel_size, stride, dilation, padding):
        super(encoder_block, self).__init__()

        self.conv1 = weight_norm(nn.Conv1d(n_inputs, n_outputs, kernel_size,
                                           stride=stride, padding=padding, dilation=dilation))
        self.chomp1 = Chomp1d(padding)
        self.relu1 = nn.LeakyReLU()

        self.conv2 = weight_norm(nn.Conv1d(n_outputs, n_outputs, kernel_size,
                                           stride=stride, padding=padding, dilation=dilation))
        self.chomp2 = Chomp1d(padding)
        self.relu2 = nn.LeakyReLU()

        self.net = nn.Sequential(self.conv1, self.chomp1, self.relu1,
                                 self.conv2, self.chomp2, self.relu2)

        self.downsample = nn.Conv1d(n_inputs, n_outputs, 1) if n_inputs != n_outputs else None
        self.init_weights()
        #self.relu = nn.LeakyReLU()

    def init_weights(self):
        self.conv1.weight.data.normal_(0, 0.01)
        self.conv2.weight.data.normal_(0, 0.01)
        if self.downsample is not None:
            self.downsample.weight.data.normal_(0, 0.01)

    def forward(self, x):
        out = self.net(x)
        res = x if self.downsample is None else self.downsample(x)
        return out + res


class FMMlayer(nn.Module):
    # FMM composition for all waves
    def __init__(self, length=600, std=1, device='cuda'):
        super(FMMlayer, self).__init__()
        self.length = length
        self.time_steps = (np.arange(length) * 2 * np.pi)/length
        self.std = std
        self.device = device

    def forward(self, m, params):
        # params has size 20
        # A, beta, omega, alpha
        b, _ = params.size()
        #error = Variable(torch.ones(b, self.length).normal_(0, self.std)).to(self.device, dtype=torch.float)
        time = Variable(torch.tensor(self.time_steps).repeat(b,1)).to(self.device, dtype=torch.float)
        out = 0
        pi = 3.141593
        for i in range(5):
            out += params[:, 4*i+0:4*i+1] * \
                   torch.cos(params[:, 4*i+1:4*i+2]*2*pi +
                             2 * torch.atan(params[:, 4*i+2:4*i+3] *
                                            torch.tan((time - torch.remainder(params[:, 4*i+3:4*i+4] * 2* pi + pi, 2*pi))/2)))
        out = out + m #+ error
        return out


class FMMSingleWavelayer(nn.Module):
    # FMM composition for one wave
    def __init__(self, length=600, std=1, device='cuda'):
        super(FMMSingleWavelayer, self).__init__()
        self.length = length
        self.time_steps = (np.arange(length) * 2 * np.pi)/length
        self.std = std
        self.device = device

    def forward(self, m, params):
        # params has size 4
        # A, beta, omega, alpha
        b, _ = params.size()
        time = Variable(torch.tensor(self.time_steps).repeat(b,1)).to(self.device, dtype=torch.float)
        out = 0
        pi = 3.141593
        out += params[:, 0:1] * \
               torch.cos(params[:, 1:2]*2*pi +
                             2 * torch.atan(params[:, 2:3] *
                                            torch.tan((time - torch.remainder(params[:, 3:4] * 2* pi + pi, 2*pi))/2)))
        out = out + m
        return out


class SingleFMMnnModel(nn.Module):
    # single wave extraction module
    def __init__(self, num_inputs, num_channels, pool_size, hidden_size, length=600, std=1, kernel_size=5, device='cuda'):
        super(SingleFMMnnModel, self).__init__()
        layers = []
        num_levels = len(num_channels)
        for i in range(num_levels):
            dilation_size = 2 ** i
            in_channels = num_inputs if i == 0 else num_channels[i - 1]
            out_channels = num_channels[i]
            layers += [encoder_block(in_channels, out_channels, kernel_size, stride=1, dilation=dilation_size,
                                     padding=(kernel_size - 1) * dilation_size)]

        self.network = nn.Sequential(*layers)
        self.pooling = nn.AdaptiveMaxPool1d(pool_size)
        self.mean_linear = nn.Linear(pool_size * num_channels[-1], hidden_size)
        self.act1 = nn.Tanh()
        self.act2 = nn.Sigmoid()
        self.recon = FMMSingleWavelayer(length=length, std=std, device=device)

    def forward(self, x):
        feature = self.network(x)
        b, _, _ = feature.size()
        pool = self.pooling(feature).view(b, -1)
        out = self.mean_linear(pool)
        mean = self.act1(out[:,0:1])
        params = self.act2(out[:,1:])
        out_signal =self.recon(mean, params)
        return mean, params, out_signal


class CascadedFMMnnModel(nn.Module):
    def __init__(self, num_inputs, num_channels, pool_size, hidden_size, num_cascade=5, length=600, std=1, kernel_size=5, device='cuda'):
        super(CascadedFMMnnModel, self).__init__()
        self.models = nn.ModuleList()
        for num in range(num_cascade):
            self.models.append(SingleFMMnnModel(num_inputs, num_channels, pool_size, hidden_size,
                                                length, std, kernel_size, device))
        self.final_recon = FMMlayer(length=length, std=std, device=device)

    def forward(self, x):
        ori = x
        final_mean = 0
        params_list = []
        ori_signal = []
        pred_signal = []
        for num in range(len(self.models)):
            ori_signal.append(x)
            mean, params, out_signal = self.models[num](x)
            pred_signal.append(out_signal.unsqueeze(1))
            x = x - out_signal.unsqueeze(1)
            params_list.append(params)
            final_mean += mean
        final_params = torch.cat(params_list, 1)
        final_signal = self.final_recon(final_mean, final_params)
        ori_signal.append(ori)
        pred_signal.append(final_signal.unsqueeze(1))
        return final_mean, final_params, ori_signal, pred_signal

