import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

# different loss functions
def reconstruction_loss(x, x_recon):
    batch_size = x.size(0)
    assert batch_size != 0
    #x_recon = F.sigmoid(x_recon)
    recon_loss = F.mse_loss(x_recon, x, size_average=False).div(batch_size)
    return recon_loss


def position_loss(params, mean, var, lamb):
    # params 20 dim
    b = params.size(0)
    alpha = params.view((b, 5, 4))[:, :, -1] #(b,4)
    loss = lamb * (alpha - mean)**2 / var
    return loss.sum()
