import os 
import neurokit2 as nk
import numpy as np
import pandas as pd
from scipy.signal import decimate, resample, resample_poly
from scipy.io import loadmat, savemat
from tqdm import tqdm

def load_recording(recording_file, header=None, leads=None, key='val'):
    recording = loadmat(recording_file)[key]
    if header and leads:
        recording = choose_leads(recording, header, leads)
    return recording


def load_header(header_file):
    with open(header_file, 'r') as f:
        header = f.read()
    return header


def get_basic_info(df, num):
    patient = df.iloc[num]
    fre = int(patient['Frequency'])
    leng = int(patient['SignalLength'])
    dataset = str(patient['Dataset'])
    filename = str(patient['FileName'])
    return fre, leng, dataset, filename


# neighbor padding the sequence to 1.2s (600 pts)
def get_seg_labels(leadii, sampling=500):
    signals, info = nk.ecg_peaks(leadii, sampling_rate=sampling, correct_artifacts=False)
    rpeaks = info["ECG_R_Peaks"]
    mean_seg = np.mean(rpeaks[1:] - rpeaks[:-1])
    segs = []
    start, end = 0, len(rpeaks)
    for r in rpeaks:
        if int(r-0.4 * mean_seg) < 0:
            start +=1
        if int(r + 0.6 * mean_seg) >= len(leadii):
            end -=1
        if int(r + 0.6 * mean_seg) < len(leadii) and int(r-0.4 * mean_seg) >= 0:
            segs.append([int(r-0.4 * mean_seg), int(r + 0.6 * mean_seg)])
    return info["ECG_R_Peaks"][start+1:end-1], segs[1:-1]


def get_median_ecg(lead_ecgf, rpeaks, labels, rcenter=250, sampling=500):
    segs_data = []
    # the first and the last one may be not the same length
    lengs = []
    for i in range(len(labels)):
        seg = labels[i]
        ecg = lead_ecgf[seg[0]:seg[1]]
        segs_data.append(ecg)
        lengs.append(len(ecg))
    lengs = np.array(lengs)
    median_leng = np.median(lengs)
    final_data = []
    for i in range(len(lengs)):
        if lengs[i] == median_leng:
            final_data.append(segs_data[i])
    final_data = np.array(final_data)
    median_ecg = np.median(final_data, axis=0)
    # padding to 600pts (500hz)
    prer_leng = int(np.median(np.array([rpeaks[i] - labels[i][0] for i in range(len(rpeaks))])))
    postr_leng = int(np.median(np.array([labels[i][1] - rpeaks[i] for i in range(len(rpeaks))])))
    if prer_leng > rcenter:
        median_ecg = median_ecg[prer_leng-rcenter:]
        prer_leng = rcenter
    if postr_leng > 600- rcenter:
        median_ecg = median_ecg[:(prer_leng + 600 - rcenter)]
        postr_leng = 600 - rcenter
    if len(median_ecg)<=600:
        s = rcenter - prer_leng
        e = 600 - rcenter - postr_leng
        median_ecg = np.pad(median_ecg, [s,e], mode='edge') 
    return median_ecg


if __name__ == '__main__':
    datafolder = ''
    info = pd.read_csv('') # read dataset information csv file
    save_folder = datafolder + 'Median_ECG/' 
    os.makedirs(save_folder, exist_ok=True)
    bad_files = []
    for num in tqdm(range(len(info))):
        # read signal 
        try:
            fre, leng, dataset, filename = get_basic_info(info, num)
            if dataset not in ['WFDB_PTB', 'WFDB_PTBXL', 'WFDB_CPSC2018', 'WFDB_CPSC2018_2']:
                continue 
            cur_save_folder = save_folder + dataset
            os.makedirs(cur_save_folder, exist_ok=True)
            fre = 500
            file =  datafolder + dataset + '/' + filename
            record = load_recording(file)
            head = load_header(file[:-4] + '.hea')
            # resample 
            if fre>500:
                drecord = np.array([decimate(sig, 2) for sig in record])
            elif fre<500:
                drecord = np.array([resample(sig, int(leng/fre*500)) for sig in record])
            else:
                drecord = record 
            # remove baseline wandering
            crecord = np.array([nk.signal.signal_filter(lead_ecg, 500, lowcut=0.5) for lead_ecg in drecord])
            # clean ecgs 
            frecord = np.array([nk.ecg_clean(lead_ecg, sampling_rate=500, method="neurokit") for lead_ecg in crecord])
            rpeaks, segs = get_seg_labels(frecord[1], sampling=500)
            median_record = [get_median_ecg(lead_ecgf, rpeaks, segs) for lead_ecgf in crecord]
            savemat(os.path.join(save_folder, filename[:-4] + '.mat'), {'val':np.array(median_record)})
        except:
            print(filename)
            bad_files.append([dataset, filename])
    savemat(os.path.join(save_folder, 'bad_files.mat'), {'bad_files':bad_files})
    
    