import numpy as np
import os
import pandas as pd
import random
import torch.utils.data
import torch
from scipy.io import loadmat

class ECGDataset(torch.utils.data.Dataset):
    '''
    Obtain normalised 12-lead single-beat ECG signal (for evaluation model)
    '''
    def __init__(self, files, lead_norm=True):
        super(ECGDataset, self).__init__()
        self.files = files
        self.lead_norm = lead_norm

    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        file = self.files[idx]
        data = loadmat(file)['val']
        out_signal, scale = self.normalisation(data)
        return file.split('/')[-1], out_signal, scale

    def normalisation(self, data):
        if self.lead_norm:
            return data / (np.max(np.abs(data), axis=1, keepdims=True) + 1e-5), (np.max(np.abs(data), axis=1, keepdims=True) + 1e-5)
        else:
            return data / (np.max(np.abs(data))), (np.max(np.abs(data)))


def get_dataset_files(data_folder, dataset_names):
    files = []
    for dataset in dataset_names:
        cur_folder = data_folder + '/' + dataset
        cur_files = os.listdir(cur_folder)
        for cf in cur_files:
            files.append(cur_folder + '/' + cf)
    return files


def get_PTBXL_train_valid_test_files(csv_path, data_folder, test_fold=1):
    '''
    Create train, valid, test 3 splits for cross validation
    test_fold: from 1-10
    '''
    # check if file exists
    files = os.listdir(data_folder)
    nums = [int(file[2:7]) for file in files]
    df = pd.read_csv(csv_path)
    df = df[df.ecg_id.isin(nums)]
    valid_fold = (test_fold+1)%10
    train_fold =  set(np.arange(1, 11)).symmetric_difference(set([test_fold, valid_fold]))
    train_nums = df[df.strat_fold.isin(train_fold)]['ecg_id'].values
    valid_nums = df[df.strat_fold==valid_fold]['ecg_id'].values
    test_nums = df[df.strat_fold==test_fold]['ecg_id'].values
    train_files = [data_folder + '/' + 'HR' + str(int(f)).zfill(5) + '.mat' for f in train_nums]
    valid_files = [data_folder + '/' + 'HR' + str(int(f)).zfill(5) + '.mat' for f in valid_nums]
    test_files = [data_folder + '/' + 'HR' + str(int(f)).zfill(5) + '.mat' for f in test_nums]
    return train_files, valid_files, test_files


class ECGLeadDataset(torch.utils.data.Dataset):
    '''
    Get 1-lead single-beat ECG signal (for training model)
    '''
    def __init__(self, files, lead=1):
        super(ECGLeadDataset, self).__init__()
        self.files = files
        self.lead = lead

    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        file = self.files[idx]
        data = loadmat(file)['val'][self.lead]
        return self.normalisation(data)

    def normalisation(self, data):
        return data/np.max(np.abs(data))
        
