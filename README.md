# ECGdecomposition

## Project Introduction 
This repository contains offical code for paper *Explainable Electrocardiogram Analysis with Wave Decomposition: Application to Myocardial Infarction Detection* in STACOM 2022. If you find the repository useful, please cite the following bibtex. Commercial use is not allowed.
```
@inproceedings{yang2022explainable,
  title={Explainable Electrocardiogram Analysis with Wave Decomposition: Application to Myocardial Infarction Detection},  
  author={Yang, Yingyu and Rocher, Marie and Moceri, Pamela and Sermesant, Maxime},  
  booktitle={International Workshop on Statistical Atlases and Computational Models of the Heart},  
  pages={221--232},  
  year={2022},  
  organization={Springer}  
}
```

## Pipeline

![pipeline](figs/pipeline.png)

*Explainable ECG analysis and classification pipeline*


## Model architecture
![model](figs/cascaded_model.png)

*Proposed cascaded model for single-beat ECG decomposition* 

