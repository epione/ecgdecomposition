import numpy as np
import pandas as pd
import os
import torch
from dataset import get_PTBXL_train_valid_test_files, ECGDataset, ECGLeadDataset
from model import CascadedFMMnnModel
from utils import reconstruction_loss, position_loss
from time import time
import sys
from scipy.io import loadmat

#======================================================
# Parameters setting
#======================================================
batch = 16
lr = 1e-4
epochs = 200
loss_lamb = [1, 1, 1, 1, 1, 2]
device = 'cuda' if torch.cuda.is_available() else 'cpu'
model_num = int(sys.argv[1])
test_fold = model_num
data_folder = ''  # datafolder
model_dir = '' + str(model_num) # save_folder
os.makedirs(model_dir, exist_ok=True)

with open(os.path.join(model_dir, 'params.txt'), 'w') as f:
    f.write('batch = ' + str(batch) + '\n')
    f.write('lr = ' + str(lr) + '\n')
    f.write('epochs = ' + str(epochs) + '\n')
    f.write('loss = ' + str(loss_lamb) + '\n')

#=====================================================
# Set dataset & initialize model
#=====================================================
print('Load dataset ...')
csv_path = data_folder + '/ptbxl_database.csv'
train_files, valid_files, test_files = get_PTBXL_train_valid_test_files(csv_path,
                                                                        data_folder + '/Median_ECG/WFDB_PTBXL',
                                                                        test_fold=test_fold)
train_ = ECGDataset(train_files)
train_loader = torch.utils.data.DataLoader(train_,
                                           batch_size=batch, shuffle=True,
                                           drop_last=True)
valid_ = ECGDataset(valid_files)
valid_loader = torch.utils.data.DataLoader(valid_,
                                           batch_size=batch, shuffle=True,
                                           drop_last=True)
print('Train files {}, Validation files {}'.format(len(train_), len(valid_)))
# set model
print('Set models ...')
model = CascadedFMMnnModel(
    num_inputs=1,
    num_channels=[128, 32],
    pool_size=32,
    hidden_size=5,
    num_cascade=5,
    length=600,
    std=1,
    kernel_size=5,
    device=device)
model.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=lr)

#===========================================================
#Distribution loss info
#===========================================================
dist_prior = loadmat('') # file path of prior information
dist_mean = torch.tensor(np.array([dist_prior['mean']])).to(device, dtype=torch.float)
dist_vars = torch.tensor(np.array([dist_prior['var']])).to(device, dtype=torch.float)

#===========================================================
# Training
#===========================================================
print('Begin training ...')
cur_val_loss = np.inf
lamb_pos = 1
for epoch in range(1, epochs+1):
    #===================================================
    # Train
    # ===================================================
    model.train()
    epoch_recon_loss = []
    epoch_total_loss = []
    epoch_step_time = []
    for i, sample in enumerate(train_loader, 1):
        step_start_time = time()
        _, signal, scale = sample
        signal = signal.to(device, dtype=torch.float).view((-1, 600))
        final_mean, final_params, ori_signal, pred_signal = model(signal.unsqueeze(1))

        recon_loss = 0
        loss_list = []
        k=0
        for o, p in zip(ori_signal, pred_signal):
            cur_loss = reconstruction_loss(o, p)
            loss_list.append(cur_loss)
            recon_loss += cur_loss * loss_lamb[k]
            k+=1
        pos_loss = position_loss(final_params, dist_mean, dist_vars, lamb_pos)
        loss_list.append(pos_loss)
        recon_loss += pos_loss

        epoch_recon_loss.append(loss_list)
        epoch_total_loss.append(recon_loss.item())

        optimizer.zero_grad()
        recon_loss.backward()  # (retain_graph=True)
        optimizer.step()

        epoch_step_time.append(time() - step_start_time)
    epoch_info = 'Training Epoch %d/%d' % (epoch, epochs)
    time_info = '%.4f sec/step' % np.mean(epoch_step_time)
    losses_info = ', '.join(['%.4e' % f for f in np.mean(epoch_recon_loss, axis=0)])
    loss_info = 'loss: %.4e (%s)'  % (np.mean(epoch_total_loss), losses_info)
    print(' - '.join((epoch_info, time_info, loss_info)), flush=True)
    with open(os.path.join(model_dir, 'training_info.csv'), 'a') as f:
        f.write(str(epoch) + ',' + losses_info + ',' + str(np.mean(epoch_total_loss)) + '\n')

    if np.mean(epoch_recon_loss, axis=0)[-2] < np.mean(epoch_recon_loss, axis=0)[-1]:
        lamb_pos *=0.1
        with open(os.path.join(model_dir, 'params.txt'), 'a') as f:
            f.write('epoch = ' + str(epoch) + ', lamb_pos = ' + str(lamb_pos) + '\n')

    with torch.no_grad():
        model.eval()
        epoch_recon_loss = []
        epoch_total_loss = []
        epoch_step_time = []
        for i, sample in enumerate(valid_loader, 1):
            step_start_time = time()
            _, signal, scale = sample
            signal = signal.to(device, dtype=torch.float).view((-1, 600))
            final_mean, final_params,  ori_signal, pred_signal = model(signal.unsqueeze(1))

            recon_loss = 0
            loss_list = []
            k = 0
            for o, p in zip(ori_signal, pred_signal):
                cur_loss = reconstruction_loss(o, p)
                loss_list.append(cur_loss)
                recon_loss += cur_loss * loss_lamb[k]
                k += 1
            pos_loss = position_loss(final_params, dist_mean, dist_vars, lamb_pos)
            loss_list.append(pos_loss)
            recon_loss += pos_loss

            epoch_recon_loss.append(loss_list)
            epoch_total_loss.append(recon_loss.item())

            epoch_step_time.append(time() - step_start_time)
        epoch_info = 'Validation Epoch %d/%d' % (epoch, epochs)
        time_info = '%.4f sec/step' % np.mean(epoch_step_time)
        losses_info = ', '.join(['%.4e' % f for f in np.mean(epoch_recon_loss, axis=0)])
        loss_info = 'loss: %.4e (%s)' % (np.mean(epoch_total_loss), losses_info)
        print(' - '.join((epoch_info, time_info, loss_info)), flush=True)
        with open(os.path.join(model_dir, 'validation_info.csv'), 'a') as f:
            f.write(str(epoch) + ',' + losses_info + ',' + str(np.mean(epoch_total_loss)) + '\n')

    #save model when validation update
    if epoch==1 or np.mean(epoch_total_loss) < cur_val_loss:
        cur_val_loss = np.mean(epoch_total_loss)
        torch.save(model.state_dict(), os.path.join(model_dir, str(epoch)+'th_model.pt'))
